# RO WarStatus script (for Regnum Haven Discord server)

This bash script tracks War Status changes from the Champions of Regnum (formerly Regnum Online) game website and sends them to a Discord channel via a Webhook.

It can track Fort and Gem captures, guess if a Realm Gate is in danger (from Relic updates) or if a Dragon Wish was made.

It can report boss respawns.

## Dependencies

Bash version 4 or above

curl

jq

diff

Since it needs to store cache data on disk make sure it has write permission to its folder.

## Setting it up

Each webhook and unique IDs for the custom emojis must be defined in a JSON-formatted file in cfg/$world.$number.json (eg haven.1.webhook). (See example folder)

Boss respawn dates are defined in cfg/$world.timers.

As it tracks changes compared to the last time this script was executed, it's meant to be ran regularly (like every minute) via systemd timers or crond. (See example folder)

Use the provided systemd unit files (systemctl start warstatus.timer) or cron to run this regularly (default is every minute) 

## Options

-w : (Mandatory) World to watch. Can be haven, ra or valhalla. Can also be "test" in which case it'll use local data instead.

-c : Clear cache files for the specified World and exit

-s : Print to the console stdout rather than send to Discord

-n : Don't update caches when running

-v : Verbose mode
