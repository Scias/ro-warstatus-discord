#!/bin/bash

# War Status monitoring script for Champions of Regnum
# Copyright (c) 2017 Scias

err () 
{
    echo ERROR: $1
    exit 1
}

fort_diff () 
{
    local fort_num realm target_realm line

    while read -r line && [[ ! -z "$line" ]]; do
    
        fort_num=$(grep -oE '[[:digit:]]+' <<< "$line")
        realm=$(grep -oE "(syrtis|ignis|alsius)" <<< "$line")
        
        if [[ "$fort_num" -le 4 ]]; then target_realm="alsius";
        elif [[ "$fort_num" -ge 5 && "$fort_num" -le 8 ]]; then target_realm="ignis";
        elif [[ "$fort_num" -ge 9 && "$fort_num" -le 12 ]]; then target_realm="syrtis"; fi
        
        msg+="${icon_flag[$realm]} **${realm^}** "
        
        if [[ "$fort_num" -eq 4 || "$fort_num" -eq 8 || "$fort_num" -eq 12 ]]; then
            if [[ "$realm" == "$target_realm" ]]; then
                msg+="**has recovered its Great Wall** ${icon_flag[$realm]}"
                rm -f "$var_folder/$realm.vuln"
            else
                msg+="**starts invading ${target_realm^}** ${icon_flag[$target_realm]}"
                touch "$var_folder/$target_realm.vuln"
            fi
        else
            msg+="has captured **${forts[$fort_num-1]}** ${icon_fort[$target_realm]}"
        fi
            
        msg+=$'\n'
        
    done <<< "$1"
}

gem_diff ()
{
    local wish=0 num gains losses realm_a realm_b line line_a line_b realm_gems

    ### Dragon wish detection
    
    if [[ -f "$var_folder/drag.wish" ]]; then
        if diff -q - "$dir/cfg/gem.reset" <<< "$cache_gem" > /dev/null 2>&1; then
            wish=$(<"$var_folder/drag.wish")
            if [[ "$wish" -eq 1 ]]; then msg+="${icon_flag[alsius]} **Alsius made a dragon wish!** :dragon:";
            elif [[ "$wish" -eq 7 ]]; then msg+="${icon_flag[ignis]} **Ignis made a dragon wish!** :dragon:";
            elif [[ "$wish" -eq 13 ]]; then msg+="${icon_flag[syrtis]} **Syrtis made a dragon wish!** :dragon:"; fi
            msg+=$'\n'
        fi
        rm -f "$var_folder/drag.wish" > /dev/null
    fi
    
    ###

    if [[ "$wish" -eq 0 ]]; then
        for i in 1 2 3
        do
            gains=$(grep '\+  ' <<< "$1" | grep "gem_$i")
            losses=$(grep '\-  ' <<< "$1" | grep "gem_$i")

            if [[ ! -z "$gains" ]]; then
                while read -r line_a && [[ ! -z "$line_a" ]]; do
                    realm_a=$(grep -oE '[[:digit:]]+[[:space:]]' <<< "$line_a")
                    if [[ "$realm_a" -le 6 ]]; then msg+="${icon_flag[alsius]} **Alsius** has taken ";
                    elif [[ "$realm_a" -ge 7 && "$realm_a" -le 12 ]]; then msg+="${icon_flag[ignis]} **Ignis** has taken ";
                    elif [[ "$realm_a" -ge 13 && "$realm_a" -le 18 ]]; then msg+="${icon_flag[syrtis]} **Syrtis** has taken "; fi
                    
                    if [[ "$i" -eq 1 ]]; then msg+="${icon_gem[0]} **Ignis Gem ";
                    elif [[ "$i" -eq 2 ]]; then msg+="${icon_gem[1]} **Alsius Gem ";
                    elif [[ "$i" -eq 3 ]]; then msg+="${icon_gem[2]} **Syrtis Gem "; fi
                                
                    if [[ "$realm_a" -eq $(( 4-i )) || "$realm_a" -eq $(( (4-i)+6 )) || "$realm_a" -eq $(( (4-i)+12 )) ]]; then msg+="#1**";
                    else msg+="#2**"; fi

                    while read -r line_b || [[ -n "$line_b" ]]; do
                        realm_b=$(grep -oE '[[:digit:]]+[[:space:]]' <<< "$line_b")
                        if ! (( (realm_a - realm_b) % 6 )); then
                            if [[ "$realm_b" -le 6 ]]; then msg+=" from **Alsius** ${icon_flag[alsius]}";
                            elif [[ "$realm_b" -ge 7 && "$realm_b" -le 12 ]]; then msg+=" from **Ignis** ${icon_flag[ignis]}";
                            elif [[ "$realm_b" -ge 13 && "$realm_b" -le 18 ]]; then msg+=" from **Syrtis** ${icon_flag[syrtis]}"; fi
                            break
                        fi
                    done <<< "$losses"
                    
                    msg+=$'\n'
                    
                done <<< "$gains"
            fi
        done
        
        ### Check if a realm got all 6 gems (Dragon wish detection)
    
        num=$((((realm_a - 1) / 6) * 6 + 1))
        if ! sed -n "$num,+5p" <<< "$cache_gem" | grep "gem_0" > /dev/null 2>&1; then echo "$num" > "$var_folder/drag.wish"; fi
        
        ### Gem status
        
        for i in 1 2 3
        do
            num=$(((i-1) * 6 + 1))
            realm_gems=$(sed -n "$num,+5p" <<< "$cache_gem" | grep -oE "gem_[1-3]" | tr -d 'gem_')
            
            if [[ "$i" -eq 1 ]]; then msg+="${icon_flag[alsius]} ";
            elif [[ "$i" -eq 2 ]]; then msg+="${icon_flag[ignis]} ";
            elif [[ "$i" -eq 3 ]]; then msg+="${icon_flag[syrtis]} "; fi
            
            if [[ -z "$realm_gems" ]]; then msg+="None";
            else
                while read -r line && [[ ! -z "$line" ]]; do
                    msg+="${icon_gem[$line-1]}"
                done <<< "$realm_gems"
            fi
            if [[ ! "$i" -eq 3 ]]; then msg+=" **|** "; fi
        done
        
        msg+=$'\n'
        
    fi
}

relic_diff ()
{
    local line
    
    # End vulnerabilty if all relics are back
    
    if [[ "$cache_relic" == *"Eferias"* && "$cache_relic" == *"Algaros"* && "$cache_relic" == *"Herbred"* ]]; then rm -f "$var_folder/syrtis.vuln"; fi
    if [[ "$cache_relic" == *"Aggersborg"* && "$cache_relic" == *"Trelleborg"* && "$cache_relic" == *"Imperia"* ]]; then rm -f "$var_folder/alsius.vuln"; fi
    if [[ "$cache_relic" == *"Menirah"* && "$cache_relic" == *"Samal"* && "$cache_relic" == *"Shaanarid"* ]]; then rm -f "$var_folder/ignis.vuln"; fi

    while read -r line && [[ ! -z "$line" ]]; do
        case "$line" in 
            *"Herbred"* | *"Eferias"* | *"Algaros"*) 
                if [[ ! -f "$var_folder/syrtis.vuln" ]]; then
                    msg+="${icon_flag[syrtis]} **Syrtis Gate is vulnerable!** $icon_warning"$'\n'
                    touch "$var_folder/syrtis.vuln"
                fi
            ;;
            *"Aggersborg"* | *"Imperia"* | *"Trelleborg"*)
                if [[ ! -f "$var_folder/alsius.vuln" ]]; then
                    msg+="${icon_flag[alsius]} **Alsius Gate is vulnerable!** $icon_warning"$'\n'
                    touch "$var_folder/alsius.vuln"
                fi
            ;;
            *"Menirah"* | *"Samal"* | *"Shaanarid"*)
                if [[ ! -f "$var_folder/ignis.vuln" ]]; then
                    msg+="${icon_flag[ignis]} **Ignis Gate is vulnerable!** $icon_warning"$'\n'
                    touch "$var_folder/ignis.vuln"
                fi
            ;;
        esac
    done <<< "$1"
}

boss_check ()
{
    local cur_time boss_time boss_date new_time new_date json step
    local -a bosses=("DaenRha" "Evendim" "Thorkul")

    cur_time=$(date -u +%s)
    step=392400
    
    if [[ ! -f "$dir/cfg/$world.timers" ]]; then err "No $world.timers file"; fi
    
    for boss in "${bosses[@]}"; do
        
        boss_date=$(jq -r ."$boss" "$dir/cfg/$world.timers")
        boss_time=$(date -u -d "$boss_date" +%s)
        
        if [[ "$cur_time" -ge "$boss_time" ]]; then
            new_time=$(( boss_time + step ))
            
            while [[ "$new_time" -le "$cur_time" ]]; do
                if [[ "$opt_verbose" -eq 1 ]]; then echo "Outdated boss time - Calculating next respawn..."; fi
                new_time=$(( new_time + step ))
            done
                
            msg+="${icon_boss[$boss]} has respawned**"
            msg+=$'\n'
            new_date=$(date -u -d "@$new_time" -Iminutes)
            json=$(jq ".$boss = \"$new_date\"" "$dir/cfg/$world.timers")
            if [[ "$opt_nosave" -eq 0 ]]; then echo "$json" > "$dir/cfg/$world.timers"; fi
        fi
    
    done
    
}

tdm_check ()
{
    local tdm_start day
    local -i hour end
    
    day=$(date "+%A")
    hour=$(date "+%-H")
    
    if [[ -f "$var_folder/tdm.end" ]]; then
        end=$(<"$var_folder/tdm.end")
        if [[ "$hour" -eq "$end" ]]; then
            msg+=":heavy_multiplication_x: The TDM session has ended"
            msg+=$'\n'
            rm -f "$var_folder/tdm.end"
        fi
    else
        tdm_start=$(jq -r ."TDM" "$dir/cfg/$world.timers")
        if grep -q "$day-$hour" <<< "$tdm_start"; then
            msg+=":crossed_swords: **A TDM session has started**"
            msg+=$'\n'
            end=$(( hour + 2 ))
            echo "$end" > "$var_folder/tdm.end"
        fi
    fi
}

decorate ()
{
    final_msg="$1"
    
    icon_fort[alsius]=$(jq -r '.alsfort' "$2")
    icon_fort[ignis]=$(jq -r '.ignfort' "$2")
    icon_fort[syrtis]=$(jq -r '.syrfort' "$2")
    
    icon_flag[alsius]=$(jq -r '.alsflag' "$2")
    icon_flag[ignis]=$(jq -r '.ignflag' "$2")
    icon_flag[syrtis]=$(jq -r '.syrflag' "$2")
    
    icon_gem[0]=$(jq -r '.igngem' "$2")
    icon_gem[1]=$(jq -r '.alsgem' "$2")
    icon_gem[2]=$(jq -r '.syrgem' "$2")
    
    icon_boss[DaenRha]=$(jq -r '.daen' "$2")
    icon_boss[Evendim]=$(jq -r '.eve' "$2")
    icon_boss[Thorkul]=$(jq -r '.thork' "$2")
    
    if [[ "$final_msg" == *":SYRFORT:"* ]]; then final_msg=$(sed -e "s/:SYRFORT:/${icon_fort[syrtis]}/g" <<< "$final_msg"); fi
    if [[ "$final_msg" == *":IGNFORT:"* ]]; then final_msg=$(sed -e "s/:IGNFORT:/${icon_fort[ignis]}/g" <<< "$final_msg"); fi
    if [[ "$final_msg" == *":ALSFORT:"* ]]; then final_msg=$(sed -e "s/:ALSFORT:/${icon_fort[alsius]}/g" <<< "$final_msg"); fi
    
    if [[ "$final_msg" == *":ALSFLAG:"* ]]; then final_msg=$(sed -e "s/:ALSFLAG:/${icon_flag[alsius]}/g" <<< "$final_msg"); fi
    if [[ "$final_msg" == *":IGNFLAG:"* ]]; then final_msg=$(sed -e "s/:IGNFLAG:/${icon_flag[ignis]}/g" <<< "$final_msg"); fi
    if [[ "$final_msg" == *":SYRFLAG:"* ]]; then final_msg=$(sed -e "s/:SYRFLAG:/${icon_flag[syrtis]}/g" <<< "$final_msg"); fi
    
    if [[ "$final_msg" == *":IGNGEM:"* ]]; then final_msg=$(sed -e "s/:IGNGEM:/${icon_gem[0]}/g" <<< "$final_msg"); fi
    if [[ "$final_msg" == *":ALSGEM:"* ]]; then final_msg=$(sed -e "s/:ALSGEM:/${icon_gem[1]}/g" <<< "$final_msg"); fi
    if [[ "$final_msg" == *":SYRGEM:"* ]]; then final_msg=$(sed -e "s/:SYRGEM:/${icon_gem[2]}/g" <<< "$final_msg"); fi
    
    if [[ "$final_msg" == *":DAENRHA:"* ]]; then final_msg=$(sed -e "s/:DAENRHA:/${icon_boss[DaenRha]} **Daen Rha/g" <<< "$final_msg"); fi
    if [[ "$final_msg" == *":EVENDIM:"* ]]; then final_msg=$(sed -e "s/:EVENDIM:/${icon_boss[Evendim]} **Evendim/g" <<< "$final_msg"); fi
    if [[ "$final_msg" == *":THORKUL:"* ]]; then final_msg=$(sed -e "s/:THORKUL:/${icon_boss[Thorkul]} **Thorkul/g" <<< "$final_msg"); fi
}

# Check for required files

dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
if [[ ! -f "$dir/cfg/gem.reset" || ! -f "$dir/cfg/config.sh" ]]; then err "Missing required files"; fi

# Variable initialisation & Constant stuff

declare -i opt_clear=0 opt_stdout=0 opt_nosave=0 opt_verbose=0 cur_min i
declare webhook msg final_msg world url cache_folder cache_html cache_fort cache_gem cache_relic diff
declare -A icon_flag=([alsius]=":ALSFLAG:" [ignis]=":IGNFLAG:" [syrtis]=":SYRFLAG:")
declare -A icon_fort=([alsius]=":ALSFORT:" [ignis]=":IGNFORT:" [syrtis]=":SYRFORT:")
declare -a icon_gem=(":IGNGEM:" ":ALSGEM:" ":SYRGEM:")
declare -A icon_boss=([DaenRha]=":DAENRHA:" [Evendim]=":EVENDIM:" [Thorkul]=":THORKUL:")
declare -a forts=("Imperia" "Aggersborg" "Trelleborg" "Alsius Wall" "Menirah" "Samal" "Shaanarid" "Ignis Wall" "Algaros" "Herbred" "Eferias" "Syrtis Wall")
icon_warning=":warning:"

# Options parsing

while getopts ":csnvhw:" opt; do
    case $opt in
    c) opt_clear=1 ;;
    s) opt_stdout=1 ;;
    n) opt_nosave=1 ;;
    v) opt_verbose=1 ;;
    w)
        if [[ "$OPTARG" != "ra" && "$OPTARG" != "haven" && "$OPTARG" != "valhalla" && "$OPTARG" != "test" ]]; then
            err "Invalid World - Must be either haven, ra or valhalla"
        else
            world="$OPTARG"
        fi
    ;;
    :) err "No World specified (haven|ra|valhalla)" ;;
    \?) err "Invalid option: -$OPTARG" ;;
  esac
done

if [[ -z "$world" ]]; then err "World required (-w haven|ra|valhalla)"; fi

# Fetch configuration

source "$dir/cfg/config.sh"

# Reset caches option

if [[ "$opt_clear" -eq 1 ]]; then
    echo "Reset Caches"
    rm -Rf "$cache_folder" "$var_folder"
    exit 0
fi

###

if [[ "$world" == "test" ]]; then 
    cache_html=$(<"$cache_folder/html2.cache")
    cache_fort=$(<"$cache_folder/fort2.cache")
    cache_gem=$(<"$cache_folder/gem2.cache")
    cache_relic=$(<"$cache_folder/relic2.cache")
else 
    cache_html=$(curl -L "$url" -s | sed '276,373!d')
    cache_fort=$(grep -oE "(alsius|ignis|syrtis)" <<< "$cache_html" | nl)
    cache_gem=$(grep -oE "gem_[0-9]" <<< "$cache_html" | nl)
    cache_relic=$(grep -oE "[[:alpha:]]* relic" <<< "$cache_html" | sort)
fi

# Check if data is valid

if [[ $(wc -l <<< "$cache_fort") != 12 || $(wc -l <<< "$cache_gem") != 18 ]]; then err "Invalid data"; fi

# Make caches if not found

if [[ ! -f "$cache_folder/html.cache" || ! -f "$cache_folder/fort.cache" || ! -f "$cache_folder/gem.cache" || ! -f "$cache_folder/relic.cache" ]]; then
    echo "First run - Create caches"
    mkdir -p "$cache_folder" "$var_folder"
    echo "$cache_html" > "$cache_folder/html.cache"
    echo "$cache_fort" > "$cache_folder/fort.cache"
    echo "$cache_relic" > "$cache_folder/relic.cache"
    echo "$cache_gem" > "$cache_folder/gem.cache"
    exit 0
fi

# Boss respawn & TDM check

cur_min=$(date +%-M)

if [[ "$cur_min" -eq 15 || "$cur_min" -eq 30 ]]; then boss_check;
elif [[ "$cur_min" -eq 0 ]]; then tdm_check; fi

# Do nothing if no change / Save HTML cache otherwise

if diff -q "$cache_folder/html.cache" - <<< "$cache_html" > /dev/null 2>&1 && [[ -z "$msg" ]]; then 
    if [[ "$opt_verbose" -eq 1 ]]; then echo "No change - Nothing to do"; fi
    exit 0
elif [[ "$opt_nosave" -eq 0 ]]; then echo "$cache_html" > "$cache_folder/html.cache"; fi

####### Forts

diff=$(diff -u "$cache_folder/fort.cache" - <<< "$cache_fort" | grep "+  ")

if [[ ! -z "$diff" ]]; then 
    if [[ "$opt_verbose" -eq 1 ]]; then echo "# Fort update"; fi
    
    fort_diff "$diff"
    
    # Update cache
    if [[ "$opt_nosave" -eq 0 ]]; then echo "$cache_fort" > "$cache_folder/fort.cache"; fi
fi

####### Gems

diff=$(diff -u "$cache_folder/gem.cache" - <<< "$cache_gem" | grep -E '(\+|-)  ' | grep -v "gem_0")

if [[ ! -z "$diff" ]]; then
    if [[ "$opt_verbose" -eq 1 ]]; then echo "# Gem update"; fi
    
    gem_diff "$diff"
    
    # Update cache
    if [[ "$opt_nosave" -eq 0 ]]; then echo "$cache_gem" > "$cache_folder/gem.cache"; fi
fi

####### Relics

diff=$(diff -u "$cache_folder/relic.cache" - <<< "$cache_relic" | grep -E '\-[[:alpha:]]')

if [[ ! -z "$diff" ]]; then
    if [[ "$opt_verbose" -eq 1 ]]; then echo "# Relic update"; fi
    
    relic_diff "$diff"
        
    # Update cache
    if [[ "$opt_nosave" -eq 0 ]]; then echo "$cache_relic" > "$cache_folder/relic.cache"; fi
fi

####### Send to Discord or stdout

if [[ ! -z "$msg" ]]; then
    if [[ "$opt_stdout" -eq 0 ]]; then
        for file in "$dir/cfg/$world".*."webhook"; do
            if [[ ! -f "$file" ]]; then err "No target webhook."; fi
            webhook=$(jq -r '.webhook' "$file")
            decorate "$msg" "$file"
            while read -r line && [[ ! -z "$line" ]]; do
                curl -s -X POST --data "{ \"content\": \"$line\" }" -H "Content-Type: application/json" "$webhook"
                sleep "$delay"
            done <<< "$final_msg"
        done
    else echo "$msg"; fi
fi
