#!/bin/bash

# URL to Champions of Regnum War Status page

url="https://www.championsofregnum.com/index.php?l=1&ref=gmf&sec=3&world=$world"

# Minimum delay between messages sent (to avoid Discord's antiflood)

delay="0.5"

# Set cache folder and webhook

cache_folder="$dir/cache/$world"
var_folder="$dir/var/$world"
#webhooks=$(<"$dir/cfg/$world.webhook") || err "Webhook file missing or invalid"
